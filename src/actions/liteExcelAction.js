const CHANGE_CELL = 'CHANGE_CELL';

function updateCell(y, x, obj) {
    return {
        type: CHANGE_CELL,
        data: { y, x, obj }
    }
}

export { CHANGE_CELL, updateCell };