import { CHANGE_CELL } from "../actions/liteExcelAction";

let m = [...new Array(20)];
m = m.map(x => [...new Array(10)].map(y => { return { val: '', type: 'general', cval: '', error: '' } }));

const initialState = {
    m
}

const fx = {
    main: (m, obj) => {
        if (obj.val[0] === '=') {
            switch (obj.val.slice(0, obj.val.indexOf('('))) {
                case '=SUM':
                    obj = fx.sum(m, obj.val, obj.type);
                    break;
                case '=AVERAGE':
                    obj = fx.average(m, obj.val, obj.type);
                    break;
                case '=CONCAT':
                    obj = fx.concat(m, obj.val);
                    break;
                case '=HYPERLINK':
                    obj = fx.hyperlink(m, obj.val);
                    break;
                default:
                    break;
            }
        } else {
            // check / filters
            let check;
            switch (obj.type) {
                case 'money':
                    check = filters.money(obj.val);
                    if (!check.valid) {
                        obj.err = 'Incorect Money format!';
                    } else {
                        obj.err = '';
                        obj.val = check.value;
                    };
                    break;

                default:
                    obj.err = '';
                    break;
            }
        }
        return obj;
    },
    sum: (m, str, type) => {
        let sum = 0,
            arr = str.slice(str.indexOf('(') + 1, str.indexOf(')')).split(';'),
            result,
            currency,
            currencyIdx;

        arr.forEach(el => {
            let obj, val;
            el = el.split(',').map(x => Number(x));
            if (el.length === 2) {
                obj = m[el[0]][el[1]]
                if (type !== obj.type) {
                    result = { val: str, err: 'Incorect type!' };
                    return
                }

                val = fx.main(m, obj).val
                    .toString() // if the sum of the sum of money
                    .replace(',', '.') // replace one ',' in num ( 1,1,1 === NaN )
                    .replace(/\s/g, ''); // remove all spaces

                if (type === 'money') {
                    currencyIdx = val.search(/[A-ZА-Я]/);
                    if (!currency && currency !== '') {
                        if (currencyIdx !== -1) {
                            currency = val.slice(currencyIdx);
                        } else {
                            currency = '';
                        }
                    } else {
                        if (currencyIdx === -1 && currency !== '') {
                            result = { val: str, err: 'Incorect currency type!' };
                            return;
                        } else if (currencyIdx !== -1 && currency !== val.slice(val.search(/[A-ZА-Я]/))) {
                            result = { val: str, err: 'Incorect currency type!' };
                            return;
                        }

                    }
                    val = val.replace(currency, '');
                }

                val = Number(val)
                if (isNaN(val)) {
                    result = { val: str, err: 'Incorect value!' };
                    return
                }
                sum += val;
            } else {
                result = { val: str, err: 'Incorect selected field!' };
                return
            }
        })

        return result ? result : { val: sum, currency }
    },
    average: (m, str, type) => {
        let obj,
            length = str.split(';').length;
        obj = fx.sum(m, str, type);
        if (!obj.err) {
            obj.val = obj.val / length;
        }
        return obj;
    },
    concat: (m, str) => {
        let conc = '',
            arr = str.slice(str.indexOf('(') + 1, str.indexOf(')')).split(';');
        arr.forEach(el => {
            let val;
            el = el.split(',').map(x => Number(x));
            if (el.length === 2) {
                val = fx.main(m, m[el[0]][el[1]]).val;
                conc += val;
            } else {
                return { val: str, err: 'Incorect selected field!' };
            }
        })
        return { val: conc };
    },
    hyperlink: (m, str) => {
        let err;
        str = str.slice(str.indexOf('(') + 1, str.indexOf(')'));

        if (!filters.hyperlink(str)) {
            err = 'Incorect hyperlink!';
        }

        return {
            val: str,
            err
        };
    }
}

const filters = {
    money: function (str) {
        let val = str.toString(),
            idx;
        val = val.toUpperCase()
        idx = val.search(/[A-ZА-Я]/);
        if (idx !== -1) {
            val = [
                Number(val.slice(0, idx).replace(/\s/g, '').replace(',', '.')),
                val.slice(idx)
            ];
        } else {
            val = [
                Number(val.replace(/\s/g, '').replace(',', '.'))
            ];
        }

        if (!val[0]) {
            return { value: str, valid: false };
        }

        val[0] = val[0].toLocaleString('ua-UA',
            {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }
        );

        if (val[1]) {
            val[0] = val[0] + ' ' + val[1];
        }

        return { value: val[0], valid: true };
    },
    hyperlink: function (str) {
        let pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator

        if (str.indexOf('=HYPERLINK(') === 0) {
            str = str.slice(str.indexOf('(') + 1, str.indexOf(')'));
        }

        return pattern.test(str);
    }
}


function liteExcelReducer(state = initialState, action) {
    switch (action.type) {
        case CHANGE_CELL:
            let { y, x, obj } = action.data;
            let m = [...state.m];

            m[y][x] = { ...m[y][x], ...obj } // ES2015
            // m[y][x] = Object.assign({}, m[y][x], obj); // ES5

            // full recalc
            m.forEach(row => row.forEach(col => {
                let result = fx.main(m, col);

                if (col.type === 'money') {
                    col.cval = result.currency ? filters.money(result.val + result.currency).value : filters.money(result.val).value;
                } else {
                    col.cval = result.val;
                }
                col.error = result.err;
            }));

            return { ...state, m };

        default:
            return state;
    }
}

export default liteExcelReducer;