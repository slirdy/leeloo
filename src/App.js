import React, { Component } from 'react';

import './App.css';
import LiteExcel from "./component/LiteExcel";


class App extends Component {
  render() {
    return (
      <div className="App">
        <LiteExcel />
      </div>
    );
  }
}

export default App;
