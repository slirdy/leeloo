import React from 'react';

function Cell({ y, x, col, selectCell, selected, lastSelected }) {
    const [iframe, setIframe] = React.useState(false);

    var className = '' + (selected ? 'selected' : '') + (lastSelected ? ' lastSelected' : '') + (col.error ? ' lite-excel-error' : '');


    function onSelect(e) {
        let ctrl = e.ctrlKey;
        selectCell(y, x, ctrl);
    }
    function onMOver() {
        setIframe(col.cval);
    }
    function onMOut() {
        setIframe(false);
    }

    return <td
        className={className}
        onClick={onSelect}
    >
        {col.type === 'hyperlink' ?
            <div
                onMouseOver={onMOver}
                onMouseOut={onMOut}
            >
                <a
                    href={col.cval}
                    target="_blank"
                    rel="noopener noreferrer"
                >{col.cval}</a>
                {iframe && <div className='lite-excel-iframe-container'>
                    <iframe scrolling="no" src={iframe} title="leFrame"/>
                </div>}
            </div>
            : col.cval}
        {col.error && <div className='lite-excel-alert'>{col.error}</div>}
    </td>;
}

export default Cell;