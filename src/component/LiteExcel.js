import React from 'react';
import { connect } from "react-redux";

import './LiteExcel.scss'

import { updateCell } from "../actions/liteExcelAction";
import Cell from "./Cell";

class LiteExcel extends React.Component {
    constructor(props) {
        super(props);

        this.tableRef = React.createRef();
        this.inputRef = React.createRef();

        this.lastSelected = '0,0';

        this.state = {
            selected: [],
            inputLine: '',
            selectLine: 'general'
        }
    }

    selectCell = (y, x, ctrl) => {
        let str = y + ',' + x,
            el = this.props.m[y][x];

        if (ctrl) {
            let selectedArr = this.state.selected,
                selectIndex = selectedArr.indexOf(str);

            if (selectIndex === -1) {
                selectedArr.push(str);
                this.setState({ selected: [...selectedArr] });
            } else {
                selectedArr.splice(selectIndex, 1);
                this.setState({ selected: [...selectedArr] });
            }
        }

        this.lastSelected = str;
        this.setState({ inputLine: el.val, selectLine: el.type });
        this.inputRef.current.focus({
            preventScroll: true
        });
    }

    unselectAll = () => {
        this.setState({ selected: [] })
    }

    inputChange = (e) => {
        let val = e.currentTarget.value,
            last = this.lastSelected;

        if (last) {
            let type = this.state.selectLine;
            if (val[0] === '=') {
                let selected = this.state.selected,
                    idx = selected.indexOf(last);   // prevent selected 'lastSelected'
                if (idx > -1) {                     //
                    selected.splice(idx, 1);        //
                }                                   //

                // first fx run
                switch (val) {
                    case '=SUM':
                        val += '(' + selected.join(';') + ')'

                        type = selected[0].split(',');
                        type = this.props.m[type[0]][type[1]].type;
                        break;
                    case '=AVERAGE':
                        val += '(' + selected.join(';') + ')'

                        type = selected[0].split(',');
                        type = this.props.m[type[0]][type[1]].type;
                        break;
                    case '=CONCAT':
                        val += '(' + selected.join(';') + ')'
                        break;
                    case '=HYPERLINK':
                        val = '=HYPERLINK()';
                        this.setState({ selectLine: 'hyperlink' })
                        type = 'hyperlink'
                        break;
                    default:
                        break;
                }
            }
            last = last.split(',').map(x => Number(x));
            val = {
                val,
                type
            }

            this.setState({ inputLine: val.val })
            this.props.updateCell(last[0], last[1], val);
        }
    }

    selectChange = (e) => {
        let val = e.currentTarget.value,
            last = this.lastSelected;
        this.setState({ selectLine: val });
        if (last) {
            last = last.split(',').map(x => Number(x));
            let el = this.props.m[last[0]][last[1]];

            switch (val) {
                // case 'money':
                //     val = this.filters.money(el.val);        // filters in reducers now
                //     el = {
                //         val: val.value,
                //         type: 'money',
                //     }
                //     this.props.updateCell(last[0], last[1], el);
                //     this.setState({ inputLine: el.val });
                //     break;

                case 'hyperlink':
                    if (el.val.indexOf('=HYPERLINK(') === -1) {
                        el = {
                            val: '=HYPERLINK(' + el.val + ')',
                            type: 'hyperlink',
                        }
                        this.props.updateCell(last[0], last[1], el);
                        this.setState({ inputLine: el.val });
                    } else {
                        this.props.updateCell(last[0], last[1], { type: val });
                    }
                    break;

                default:
                    this.props.updateCell(last[0], last[1], { type: val });
                    break;

            }
        }
    }

    componentDidMount() {
        // TEST
    }



    render() {
        return <div className='lite-excel-container'>
            <div>
                <button onClick={this.unselectAll}>Unselect all</button>
            </div>
            <div className='selected-info'>
                <label><b>Selected fields</b></label>
                <span>
                    {
                        this.state.selected.join(' / ')
                    }
                </span>
            </div>
            <small><b>Hold CTRL for cell selection</b></small>
            <table
                ref={this.tableRef}
            >
                <tbody>
                    {this.props.m.map((row, row_idx) => <tr key={row_idx}>
                        {row.map((col, col_idx) => {
                            return <Cell
                                y={row_idx}
                                x={col_idx}
                                col={col}
                                selectCell={this.selectCell}
                                selected={this.state.selected.includes(row_idx + ',' + col_idx)}
                                lastSelected={this.lastSelected === (row_idx + ',' + col_idx)}
                                //
                                key={col_idx}
                            />
                        })}
                    </tr>
                    )}
                </tbody>
            </table>
            <div className='lite-excel-formal'>
                <span>
                    <input
                        onChange={this.inputChange}
                        ref={this.inputRef}
                        list='leFormalInputList'
                        value={this.state.inputLine}
                        title='Formal line'
                    />
                    <datalist id='leFormalInputList'>
                        <option value='=SUM'>Сумма</option>
                        <option value='=AVERAGE'>Среднее значение</option>
                        <option value='=CONCAT'>Объединение</option>
                        <option value='=HYPERLINK'>Гиперссылка</option>
                    </datalist>
                </span>
                <span>
                    <select
                        onChange={this.selectChange}
                        value={this.state.selectLine}
                        title='Type'
                    >
                        <option value='general'>General</option>
                        <option value='money'>Money</option>
                        <option value='hyperlink'>Hyperlink</option>
                    </select>
                </span>
            </div>
        </div>
    }
}

const mapStateToProps = state => {
    return {
        m: state.m
    }
}

const mapDispathToProps = dispatch => {
    return {
        updateCell: (y, x, obj) => dispatch(updateCell(y, x, obj))
    }
}

export default connect(mapStateToProps, mapDispathToProps)(LiteExcel);